colors = [["#282a36", "#282a36"], # Background
          ["#44475a", "#44475a"], # Current Line
          ["#f8f8f2", "#f8f8f2"], # Foreground
          ["#6272a4", "#6272a4"], # Comment
          ["#bd93f9", "#bd93f9"], # Purple
          ["#ff79c6", "#ff79c6"], # Pink
          ["#8be9fd", "#8be9fd"], # Cyan
          ["#ffb86c", "#ffb86c"], # Orange
          ["#50fa7b", "#50fa7b"], # Green
          ["#ff5555", "#ff5555"], # Red
          ["#f1fa8c", "#f1fa8c"]] # Yellow
