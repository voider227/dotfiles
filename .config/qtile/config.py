import os
import re
import socket
import subprocess
from libqtile.config import KeyChord, Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

mod = "mod4"                                       # Sets mod key to SUPER/WINDOWS: mod1 = alt, mod4 = super
myTerm = "alacritty"                                 # My terminal of choice
myFm = "vifm"
Run = "dmenu_run -fn 'Inconsolata 12' -nb '#282a36' -nf '#f8f8f2' -p 'Run: '"
myConfig = "/home/void/.config/qtile/config.py"    # The Qtile config file location

keys = [
         ### The essentials
         Key([mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
         Key([mod, "shift"], "Return",
             lazy.spawn(Run),
             desc='Rofi Run Launcher'
             ),
         Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod, "shift"], "q",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
        Key([mod, "shift"], "e",
             lazy.shutdown(),
             desc='shutdown Qtile'
             ),

         ### Treetab controls
         Key([mod, "control"], "k",
             lazy.layout.section_up(),
             desc='Move up a section in treetab'
             ),
         Key([mod, "control"], "j",
             lazy.layout.section_down(),
             desc='Move down a section in treetab'
             ),
         ### Window controls
         Key([mod], "j",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "k",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod], "h",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod], "l",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod, "shift"], "m",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([mod, "shift"], "space",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
         Key([mod], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         Key([mod, "control"], "Return",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
        ### My applications launched with SUPER + SHIFT + KEY
        Key([mod], "w",
             lazy.spawn("librewolf"),
             desc='Launches browser'
             ),
        Key([mod, "shift"], "w",
             lazy.spawn("brave"),
             desc='Launches brave'
             ),
        Key([mod, "shift"], "b",
             lazy.spawn(myTerm+" -e bluetoothctl"),
             desc='Launches bluetooth control'
             ),
        Key([mod, "shift"], "v",
             lazy.spawn(myTerm+" -e alsamixer"),
             desc='Launches alsamixer'
             ),
        Key([mod, "shift"], "g",
             lazy.spawn(" -e bpytop"),
             desc='Launches task manager'
             ),
        Key([mod, "shift"], "p",
             lazy.spawn("pavucontrol"),
             desc='Launches volume control'
             ),
        Key([mod, "shift"], "d",
             lazy.spawn(myTerm+" -e vifm"),
             desc='Launches file manager'
             ),
        Key([mod, "shift"], "n",
             lazy.spawn(myTerm+" -e joplin-cli"),
             desc='Launches joplin (terminal)'
             ),
        ### Exit, logoff, suspend, ...       
        Key([mod, "shift"], "l",
             lazy.spawn("betterlockscreen -l blur"),
             desc='Locks the screen'
             ),
]

group_names = [("WWW", {'layout': 'monadtall'}),
               ("DEV", {'layout': 'monadtall'}),
               ("SYS", {'layout': 'monadtall'}),
               ("DOC", {'layout': 'monadtall'}),
               ("VBOX", {'layout': 'monadtall'}),
               ("CHAT", {'layout': 'monadtall'}),
               ("MUS", {'layout': 'monadtall'}),
               ("VID", {'layout': 'monadtall'}),
               ("GFX", {'layout': 'floating'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 6,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }

layouts = [
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Tile(shift_windows=True, **layout_theme),
    layout.Stack(num_stacks=2),
    layout.TreeTab(
         font = "Aldrich",
         fontsize = 10,
         sections = ["FIRST", "SECOND"],
         section_fontsize = 11,
         bg_color = "141414",
         active_bg = "90C435",
         active_fg = "000000",
         inactive_bg = "384323",
         inactive_fg = "a0a0a0",
         padding_y = 5,
         section_top = 10,
         panel_width = 320
         ),
    layout.Floating(**layout_theme)
]
# Format
#0> panel background
#1> background for current screen tab
#2> font color for group names
#3> border line color for current tab
#4> border line color for other tab and odd widgets
#5> color for the even widgets
#6> window name

# Dracula
colors = [["#282a36", "#282a36"], # Background
          ["#44475a", "#44475a"], # Current Line
          ["#f8f8f2", "#f8f8f2"], # Foreground
          ["#6272a4", "#6272a4"], # Comment
          ["#bd93f9", "#bd93f9"], # Purple
          ["#ff79c6", "#ff79c6"], # Pink
          ["#8be9fd", "#8be9fd"], # Cyan
          ["#ffb86c", "#ffb86c"], # Orange
          ["#50fa7b", "#50fa7b"], # Green
          ["#ff5555", "#ff5555"], # Red
          ["#f1fa8c", "#f1fa8c"]] # Yellow

#prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Noto Sans Bold",
    fontsize = 12,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                ### Logo
                widget.Sep(
                       linewidth = 0,
                       padding = 3,
                       foreground = colors[2],
                       background = colors[0]
                       ), 
                widget.Image(
                       filename = "~/.config/qtile/icons/python.png",
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(Run)}
                       ),
                ### Groups
                widget.Sep(
                       linewidth = 0,
                       padding = 3,
                       foreground = colors[2],
                       background = colors[0]
                       ), 
                widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 9,
                       margin_y = 3,
                       margin_x = 0, 
                       padding_y = 4,
                       padding_x = 3,  
                       borderwidth = 3,   
                       active = colors[4],  
                       inactive = colors[2],
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0]
                       ),
                ### Window Name
                widget.Sep(
                       linewidth = 0,
                       padding = 20,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
                ### Net
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.Net(
                       interface = "wlp1s0",
                       format = '{down}',
                       foreground = colors[2],
                       background = colors[0],
                       padding = 2,
                       fontsize = 11
                       ),
                widget.TextBox(
                       text = "",
                       padding = 2,
                       foreground = colors[2],
                       background = colors[0],
                       fontsize = 25
                       ),
                widget.Net(
                       interface = "wlp1s0",
                       format = '{up}   ',
                       foreground = colors[2],
                       background = colors[0],
                       padding = 0,
                       fontsize = 11
                       ),
                ### Volume
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.TextBox(
                       text = "",
                       padding = 4,
                       foreground = colors[10],
                       background = colors[0],
                       fontsize = 25
                       ),
                widget.Volume(
                       background = colors[0]
                       ),
                ### Updates
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.TextBox(
                       text = "",
                       padding = 4,
                       foreground = colors[9],
                       background = colors[0],
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm+' -e sudo pacman -Syu')},
                       fontsize = 25
                       ),
                widget.CheckUpdates(
                       update_interval = 60,
                       foreground = colors[2],
                       background = colors[0],
                       no_update_string = 'n/a',
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm+' -e sudo pacman -Syu')},
                       distro = 'Arch'
                       ),
                ### Disk Usage
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.TextBox(
                       text = "",
                       padding = 4,
                       foreground = colors[7],
                       background = colors[0],
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm+' -e'+myFm)},
                       fontsize = 25
                       ),
                widget.DF(
                       background = colors[0],
                       partition = "/home",
                       format = '{f}G',
                       visible_on_warn = False,
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm+' -e'+myFm)},
                       update_interval = 5
                       ),
                ### Wlan
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.TextBox(
                       text = "",
                       padding = 4,
                       foreground = colors[5],
                       background = colors[0],
                       fontsize = 25
                       ),
                widget.Wlan(
                       padding = 4,
                       background = colors[0],
                       interface = 'wlp1s0',
                       format = '[{essid}:{percent:2.0%}]',
                       disconnected_message = ''
                       ),
                ### Battery
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.TextBox(
                       text = "",
                       padding = 4,
                       foreground = colors[8],
                       background = colors[0],
                       fontsize = 25
                       ),
                widget.Battery(
                       padding = 4,
                       background = colors[0],
                       format = '{char} {percent:1.0%}',
                       low_foreground = colors[3],
                       update_interval = 5
                       ),
                ### Layout
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = colors[0],                                                          
                       background = colors[0],                                                          
                       padding = 0,                                                                     
                       scale = 0.7                                                                      
                       ),                                                                               
                widget.CurrentLayout(
                       foreground = colors[2],
                       background = colors[0],
                       padding = 5                                                                      
                       ),
                ### Clock
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.Clock(
                       padding = 4,
                       foreground = colors[2],
                       background = colors[0],
                       format = "%a, %b %d  [ %-I:%M:%S %p ]"
                       ),
                ### Systray
                widget.Sep(
                       linewidth = 1,
                       padding = 5,
                       foreground = colors[4],
                       background = colors[0]
                       ), 
                widget.Systray(
                       background = colors[0],
                       padding = 5
                       ),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
