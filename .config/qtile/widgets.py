##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Ubuntu Bold",
    fontsize = 12,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                       linewidth = 0,
                       padding = 3,
                       foreground = colors[2],
                       background = colors[0]
                       ), 
                widget.Image(
                       filename = "~/.config/qtile/icons/python.png",
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('dmenu_run -p "Run: "')}
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 3,
                       foreground = colors[2],
                       background = colors[0]
                       ), 
                widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 9,
                       margin_y = 3,
                       margin_x = 0, 
                       padding_y = 4,
                       padding_x = 3,  
                       borderwidth = 3,   
                       active = colors[4],  
                       inactive = colors[2],
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 40,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
                ### Net down/up
                widget.TextBox( 
                       text = "",
                       padding = -1,
                       foreground = colors[4],
                       background = colors[0],
                       fontsize = 50
                       ),
                widget.Net(
                       interface = "wlp1s0",
                       format = '{down}',
                       foreground = colors[2],
                       background = colors[4],
                       padding = 4,
                       fontsize = 11
                       ),
                widget.TextBox(
                       text = "",
                       padding = 0,
                       foreground = colors[2],
                       background = colors[4],
                       fontsize = 20
                       ),
                widget.Net(
                       interface = "wlp1s0",
                       format = '{up}   ',
                       foreground = colors[2],
                       background = colors[4],
                       padding = 0,
                       fontsize = 11
                       ),
                ### Volume
                widget.TextBox( 
                       text = "",
                       padding = -1,
                       foreground = colors[5],
                       background = colors[4],
                       fontsize = 50
                       ),
                widget.Volume(
                       background = colors[5]
                       ),
                ### Updates
                widget.TextBox( 
                       text = "",
                       padding = -1,
                       foreground = colors[4],
                       background = colors[5],
                       fontsize = 50
                       ),
                widget.TextBox(
                       text = "⟳",
                       padding = 4,
                       foreground = colors[2],
                       background = colors[4],
                       fontsize = 20
                       ),
                widget.CheckUpdates(
                       update_interval = 60,
                       foreground = colors[2],
                       background = colors[4],
                       colour_have_updates = colors[3],
                       no_update_string = 'n/a',
                       distro = 'Arch'
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 3,
                       foreground = colors[2],
                       background = colors[4]
                       ), 

                ### Disk Usage
                widget.TextBox( 
                       text = "",
                       padding = -1,
                       foreground = colors[5],
                       background = colors[4],
                       fontsize = 50
                       ),
                widget.TextBox(
                       text = "",
                       padding = 4,
                       foreground = colors[2],
                       background = colors[5],
                       fontsize = 20
                       ),
                widget.DF(
                       background = colors[5],
                       partition = "/home",
                       format = '{f}G',
                       visible_on_warn = False,
                       update_interval = 5
                       ),
                ### Wlan
                widget.TextBox( 
                       text = "",
                       padding = -1,
                       foreground = colors[4],
                       background = colors[5],
                       fontsize = 50
                       ),
                widget.Wlan(
                       padding = 4,
                       background = colors[4],
                       interface = 'wlp1s0',
                       format = '[{essid}:{percent:2.0%}]',
                       disconnected_message = ''
                       ),
                ### Battery
                widget.TextBox( 
                       text = "",
                       padding = -1,
                       foreground = colors[5],
                       background = colors[4],
                       fontsize = 50
                       ),
                widget.Battery(
                       padding = 4,
                       background = colors[5],
                       format = '{char} {percent:1.0%}',
                       low_foreground = colors[3],
                       update_interval = 5
                       ),
                ### Layout
                widget.TextBox(
                       text = "",
                       padding = -1,
                       foreground = colors[4],
                       background = colors[5],
                       fontsize = 50
                       ),
                widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = colors[0],                                                          
                       background = colors[4],                                                          
                       padding = 0,                                                                     
                       scale = 0.7                                                                      
                       ),                                                                               
                widget.CurrentLayout(
                       foreground = colors[2],
                       background = colors[4],
                       padding = 5                                                                      
                       ),
                ### Clock with systray
                widget.TextBox(
                       text = "",
                       padding = -1,
                       foreground = colors[5],
                       background = colors[4],
                       fontsize = 50
                       ),
                widget.Clock(
                       padding = 4,
                       foreground = colors[2],
                       background = colors[5],
                       format = "%a, %b %d  [ %-I:%M:%S %p ]"
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 1,
                       foreground = colors[0],
                       background = colors[5]
                       ),
                widget.Systray(
                       background = colors[5],
                       padding = 5
                       ),
            ],
            24,
        ),
    ),
]


