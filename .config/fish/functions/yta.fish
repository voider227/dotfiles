# Defined in - @ line 1
function yta --wraps='youtube-dl -x --audio-quality 0 --recode-video mp4' --wraps='youtube-dl -x --audio-quality 0 --recode-video mp3' --wraps='youtube-dl -x --audio-quality 0 --audio-format mp3' --description 'alias yta=youtube-dl -x --audio-quality 0 --audio-format mp3'
  youtube-dl -x --audio-quality 0 --audio-format mp3 $argv;
end
