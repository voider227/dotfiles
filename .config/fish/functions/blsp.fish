# Defined in - @ line 1
function blsp --wraps='sudo systemctl stop bluetooth.service' --description 'alias blsp=sudo systemctl stop bluetooth.service'
  sudo systemctl stop bluetooth.service $argv;
end
