#!/bin/bash

declare options=("alacritty
i3
neovim
picom
qtile
sxhkd
termite
vifm
quit")

choice=$(echo -e "${options[@]}" | dmenu -fn 'Inconsolata 12' -nb '#282a36' -nf '#f8f8f2' -l 20 -p 'Config: ')

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	alacritty)
		choice="$HOME/.config/alacritty/alacritty.yml"
	;;
	i3)
		choice="$HOME/.config/i3/config"
	;;
	neovim)
		choice="$HOME/.config/nvim/init.vim"
	;;
	picom)
		choice="$HOME/.config/picom/picom.conf"
	;;
	qtile)
		choice="$HOME/.config/qtile/config.py"
	;;
	sxhkd)
		choice="$HOME/.config/sxhkd/sxhkdrc"
	;;
	termite)
		choice="$HOME/.config/termite/config"
	;;
	vifm)
		choice="$HOME/.config/vifm/vifmrc"
	;;
	*)
		exit 1
	;;
esac
alacritty -e nvim "$choice"
# emacsclient -c -a emacs "$choice"

