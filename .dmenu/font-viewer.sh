#! /bin/bash
choice=$(fc-list | awk -F: '{print $1}' | tr -d ':' | dmenu -fn 'Inconsolata 12' -nb '#282a36' -nf '#f8f8f2' -l 20 -p 'Fonts: ')

display "$choice"
